import HomePage from '../pages/home-page';

class HomePageFlow{
    welcomeModal_clickNotNow(){
        let homePage = new HomePage();
        homePage._buttonNotNow.waitForDisplayed(6000);
        homePage._buttonNotNow.click();
    }

    setupStaff(){
        let homePage = new HomePage();
        browser.pause(5000);       
        homePage._setupNavigationPane.waitForDisplayed(90000);
        homePage._setupNavigationPane.click();
        browser.pause(5000);
        homePage._setupPracticeStaff.waitForDisplayed(90000);
        homePage._setupPracticeStaff.click();    
    }
}

export default new HomePageFlow();