import LoginPage from '../pages/login-page'
import {TestConstants} from '../data/constants';
import {UserDetails} from '../data/user-details';
import { assert } from 'chai';  

var errormsg;

class LoginFlow{
    accessURL(){        
        browser.url(TestConstants.onvioUrl);
        browser.maximizeWindow();
        new LoginPage();
    } 

    successfulLogin(){
        let loginPage = new LoginPage();
        browser.pause(5000);
        loginPage._inputUsername.waitForDisplayed(90000);
        loginPage._inputUsername.clearValue();
        loginPage._inputPassword.clearValue();
        loginPage._inputUsername.setValue(UserDetails.username);
        loginPage._inputPassword.setValue(UserDetails.password);
        browser.pause(5000);
        loginPage._buttonSignIn.waitForDisplayed(90000);
        loginPage._buttonSignIn.click();
        browser.pause(5000);
        loginPage._buttonRemindMeLater.waitForDisplayed(90000);
        loginPage._buttonRemindMeLater.click();
    }

    inputInvalidUsername(){
        let loginPage = new LoginPage();
        let r = Math.random().toString(36).substring(7);
        console.log("random", r);
        browser.refresh();
        loginPage._inputUsername.clearValue();
        loginPage._inputPassword.clearValue();
        loginPage._inputUsername.setValue(r);
        loginPage._inputPassword.setValue(r);
        loginPage._buttonSignIn.click();
        browser.pause(5000);
        loginPage._invalidUsernameError.waitForDisplayed(7000);
        errormsg = loginPage._invalidUsernameError.getText();
        assert.equal(errormsg, "Please provide a valid Email Address (Thomson Reuters ID).");
    }

    inputInvalidPassword(){
        let loginPage = new LoginPage();
        let r = Math.random().toString(36).substring(7);
        console.log("random", r);
        browser.refresh();
        loginPage._inputUsername.clearValue();
        loginPage._inputPassword.clearValue();
        loginPage._inputUsername.setValue(UserDetails.username)
        loginPage._inputPassword.setValue(r)
        loginPage._buttonSignIn.click();
        browser.pause(5000);
        loginPage._invalidPasswordError.waitForDisplayed(7000);
        //loginPage._invalidPasswordError.waitForExist(5000);
        errormsg = loginPage._invalidPasswordError.getText();
        assert.equal(errormsg, "Incorrect Email Address or Password. Please try again.");
    }

    inputPasswordLockingAccount(){       
        let loginPage = new LoginPage();
        var i;
        let r = Math.random().toString(36).substring(7);
        //console.log("random", r);
        loginPage._inputUsername.waitForDisplayed(7000);
        loginPage._inputUsername.clearValue();
        loginPage._inputPassword.clearValue();
        loginPage._inputUsername.setValue(UserDetails.username);
        loginPage._inputPassword.setValue(r);
        loginPage._buttonSignIn.click();
        loginPage._invalidPasswordError.waitForDisplayed(7000);
        browser.refresh();
    }       

    validateLockAccount(){
        let loginPage = new LoginPage();
        browser.pause(5000);
        loginPage._inputUsername.waitForDisplayed(7000);
        loginPage._inputUsername.clearValue();
        loginPage._inputPassword.clearValue();
        loginPage._inputUsername.setValue(UserDetails.username);
        loginPage._inputPassword.setValue(UserDetails.password);
        loginPage._buttonSignIn.click();
        browser.pause(5000);
        loginPage._invalidPasswordError.waitForDisplayed(7000);
        errormsg = loginPage._invalidPasswordError.getText();
        assert.equal(errormsg, "Too many sign in attempts. Please check for unlock email or try later.");
    }
        
    inputUserInfo(){
        let loginPage = new LoginPage();
        loginPage._inputUsername.clearValue();
        loginPage._inputPassword.clearValue();
        loginPage._inputUsername.setValue(UserDetails.username);
        loginPage._inputPassword.setValue(UserDetails.password);
    }

    signIn(){
        let loginPage = new LoginPage();
        loginPage._buttonSignIn.click();
    }

    clickRemindMeLater(){
        let loginPage = new LoginPage();
        loginPage._buttonRemindMeLater.click();
    }
}

export default new LoginFlow();