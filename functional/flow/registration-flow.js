import RegistrationPage from '../pages/registration-page';
import { UserDetails } from '../data/user-details';

class RegistrationFlow{
    addStaff(){
        let staffPage = new RegistrationPage();
        staffPage._addButton.waitForDisplayed(90000);
        staffPage._addButton.click();       
        staffPage._generalId.waitForDisplayed(90000);
        staffPage._generalId.setValue(UserDetails.id);
        staffPage._fullName.setValue(UserDetails.fullname);
        staffPage._emailAddress.setValue(UserDetails.email);
        staffPage._saveButton.click();
    }

    editStaff(){
        let staffPage = new RegistrationPage();
        staffPage._searchBox.waitForDisplayed(90000);
        staffPage._searchBox.setValue(UserDetails.id);
        browser.pause(5000);
        staffPage._checkBox.click();
        staffPage._editButton.click();
        staffPage._fullName.waitForDisplayed();
        staffPage._fullName.setValue("EDIT");
        staffPage._saveButton.click();
        browser.pause(5000);
    }

    deleteStaff(){
        let staffPage = new RegistrationPage();
        staffPage._searchBox.waitForDisplayed(90000);
        staffPage._searchBox.setValue(UserDetails.id);
        browser.pause(5000);
        staffPage._checkBox.click(); 
        staffPage._deleteButton.click();
        staffPage._deleteModal.waitForDisplayed(90000);
        staffPage._deleteBtnModal.click();
        staffPage._searchBox.waitForDisplayed(90000);
        staffPage._searchBox.setValue(UserDetails.id);
        browser.pause(5000);
        staffPage._noResults.waitForDisplayed(90000);
        errormsg = staffPage._noResults.getText();
        assert.equal(errormsg, "Your search did not return any results.");
    }
}

export default new RegistrationFlow();