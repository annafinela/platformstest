class HomePage{

     get _welcomeModal()            {return $('//div[@class="modal-content"]');}

    get _buttonStartTour()         {return $('//button[contains(text(),"Start tour")]');}
    
    get _buttonNotNow()            {return $('//button[contains(text(),"Not now. Ask me later")]');}

    get _buttonNoThanks()          {return $('//button[contains(text(),"No thanks")]');}

    get _myCenterHeader()          {return $('//div[@qe-id="dashboard-section"]//h2[contains(text(),"My Center")]');}

    get _setupNavigationPane()     {return $('//li[@id="bm-setup"]//span[contains(text(),"Setup")]')}
    
    get _setupPracticeStaff()      {return $('//a[contains(text(),"Staff")]');}
}

export default HomePage;