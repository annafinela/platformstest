class LoginPage{

    get _onvioHeader()         {return $('//span[@class="bm-product-name ng-scope"]');}

    get _loginBackground()     {return $('//div[@class="bm-flex-wrap bm-onvio bm-grass-back"]');}

    get _inputUsername()       {return $('//input[@name="uid"]');}

    get _inputPassword()       {return $('//input[@name="pwd"]');}

    get _buttonSignIn()        {return $('//button[@name="signinBtn"]');}

    get _buttonRemindMeLater() {return $('//button[contains(text(),"Remind Me Later")]');}

    get _invalidUsernameError() {return $('//span[@class="trta1-icon-user"]//div[@class="trta1-validation-msg"]');}

    get _invalidPasswordError() {return $('//form[@class="trta1-active-form trta1-submitted trta1-error"]//div[@class="trta1-form-msg"]');}

    get _Home()                 {return $('//a[@data-qe-id="Bluemoon.Shell.Navigation.Home"]');}
}

export default LoginPage;