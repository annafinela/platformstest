class RegistrationPage{

    get _staffHeader()          {return $('//h1[contains(text(),"Staff")]');}

    get _addButton()            {return $('//ul[@class="nav navbar-nav"]//a[@data-qe-id="EmployeeGrid-add-button"]');}

    get _editButton()           {return $('//ul[@class="nav navbar-nav"]//a[@data-qe-id="EmployeeGrid-edit-button"]');}

    get _deleteButton()         {return $('//ul[@class="nav navbar-nav"]//a[@data-qe-id="EmployeeGrid-delete-button"]');}
    
    get _saveButton()           {return $('//button[@id="employeeForm.save"]');}

    get _searchBox()            {return $('//input[@data-qe-id="EmployeeGrid-toolbar-search"]');}
    
    get _generalTab()           {return $('//a[contains(text(),"General")]');}

    get _generalFormHeader()    {return $('//h3[contains(text(),"General")]');}
    
    get _generalId()            {return $('//input[@id="code"]');}

    get _fullName()             {return $('//input[@id="fullName"]');}

    get _emailAddress()         {return $('//input[@id="emailAddress_KEY__0"]');}

    get _checkBox()             {return $('//div[@class="wj-topleft"]//i[@class="bento-flex-grid-checkbox"]');}

    get _deleteModal()          {return $('//div[@class="modal-content"]');}

    get _deleteBtnModal()       {return $('//button[@id="bmpConfirmationDialog.ok"]');}

    get _noResults()            {return $('//p[@data-qe-id="EmployeeGrid-none-message"]');}
}

export default RegistrationPage;