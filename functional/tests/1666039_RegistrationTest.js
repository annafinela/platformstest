import LoginFlow from '../flow/login-flow';
import HomepageFlow from '../flow/home-page-flow';
import RegistrationFlow from '../flow/registration-flow';

describe ('Onvio Site Test Automation - Registration', function(){

    it('should be able to login successfully', function(){
        LoginFlow.accessURL();
        LoginFlow.successfulLogin();
    });

    it('should be able to create/add Staff', function(){
        HomepageFlow.setupStaff();
        RegistrationFlow.addStaff();
    });

    it('should be able to edit Staff', function(){
        RegistrationFlow.editStaff();
    });

    it('should be able to delete Staff', function(){
        RegistrationFlow.deleteStaff();
    });
});