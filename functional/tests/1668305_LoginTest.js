import LoginFlow from '../flow/login-flow';

describe ('Onvio Site Test Automation - Login', function(){
        
    it('should be able to access URL', function(){
        LoginFlow.accessURL();
        LoginFlow.successfulLogin();
        browser.refresh();
    });

    /*it('should be not be able to login with invalid email address', function(){
        LoginFlow.accessURL();
        LoginFlow.inputInvalidUsername();
    });

    it('should be not be able to login with invalid password', function(){
        LoginFlow.inputInvalidPassword();
    });

    it('should be not be locked out if user inputted incorrect password 10 or more times', function(){       
        var i;
        for (i=0;i<9;i++){
            LoginFlow.inputPasswordLockingAccount();
            //console.log(i);
        }
        
        LoginFlow.validateLockAccount();
    });*/

});